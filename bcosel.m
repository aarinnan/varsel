function [model, evolve] = bcosel( X, Y, opt)
%[model, evolve] = bcosel( X, Y, opt)
%
% Variable selection using b-filter like methods (should eventually be
% incorporated into VARSEL)
%
%INPUT:
% X       X-matrix, samples in rows
% Y       Y-matrix, samples in rows
% opt     Type 'opt = bcosel' for default
%
%OUTPUT:
% model   The final model
% evolve  Details of how the model has involved
%
% See also: cv, cvind, rpls, sr, plsuve, ipq, jkpls, vip

% AAR 100111 Prepared it for use with 
% AAR 060110

if nargin == 0
    model.PP.met = {'mc'};
    model.PP.set = NaN;
    model.nLV = 3;
    model.Method = 'rpls';
    model.Val.met = 'CV';
    model.Val.ind = NaN;
    model.Val.rep = NaN;
    model.Val.Info = {'met - Validation method: CV, Boot or Test'; ...
        'ind - Index for CV or Test, or number of Bootstrap samples'; ...
        'rep - Sample number. Replicates have same number'};
    model.Set = [-1 -1];
    model.Info = {'PP - Preprocessing (see ''prepro'')'; 'nLV - Number of factors'; ...
        'Method - rPLS, jk, uve, ipw, sr, vip, ipls, gapls or pv'; 'Ind - Variable index used for iPLS and GA-PLS';...
        'Val - See Val.Info'; 'Set - [<Number of iterations> <Limit for selection (only applicable for some of the methods)>]'};
    return
end

if ~isstruct( opt)
    error( '''opt'' should be struct-array')
end

% if sign( opt.Set(1)) == 1 % A fixed number of iterations
%     for ci = 1:opt.Set(1)
%         switch lower( opt.Val.met)
%             case 'cv'
%                 mod = cv( X, Y, opt);
%             case 'boot'
%                 b = apls( X, Y, opt.nLV);
%             case 'test'
%                 b = apls( X, Y, opt.nLV);
%             case 'none'
                
switch opt.Method
    case 'sr'
        [model, evolve] = sr( X, Y, opt);
    case 'rpls'
        [model, evolve] = rpls( X, Y, opt);
    case 'uve'
        [model, evolve] = plsuve( X, Y, opt);        
    case 'ipw'
        [model, evolve] = ipw( X, Y, opt);
    case 'jk'
        [model, evolve] = jkpls( X, Y, opt);
    case 'vip'
        [model, evolve] = vip( X, Y, opt);
    case 'ipls'
        [model, evolve] = ipls( X, Y, opt);
    case 'gapls'
        [model, evolve] = gapls( X, Y, opt);
    case 'pv'
        [model, evolve] = pv( X, Y, opt);
    otherwise
        error( 'That variable selection method has not been implemented yet')
end