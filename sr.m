function [ model, evolve] = sr( X, Y, opt)
% [ model, evolve] = sr( X, Y, opt)
%
%INPUT:
% X      X-Matrix, with samples in rows
% Y      Y-matrix, only good for one Y at the moment
% opt    Type opt = sr to get default settings
%
%OUTPUT:
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

% AAR 060111

%Based on the article: Tarja Rajalahti, Reidar Arneberg, Frode S. Berven,
%    Kjell-Morten Myhr, Rune J. Ulvik, Olav M. Kvalheim (2009): Biomarker
%    discovery in mass spectral profiles by means of selectivity ratio
%    plot, Chemometrics and Intelligent Laboratory Systems, 95, 35-48

if nargin == 0
    model = bcosel;
    model.Method = 'sr';
    model.Set = [Inf 3];
    model.Info{6} = 'Set - Two numbers. [Number of iterations, Limit of removing a variable]';
    model.Info{7} = '      Set(1) = Inf: until no more model improvement, default';
    return
end

opt.Val.met = lower( opt.Val.met);
if opt.Val.met == 'cv' %#ok<STCMP>
    if isnan( opt.Val.ind)
        opt.Val.ind = 1:size( X, 1);
    end
end

if opt.Set(2) < 0
    opt.Set(2) = 3;
end

[r, c] = size( X);
test = true;
evolve.ind = false( 1, c);
evolve.num = 0;
evolve.rms = sqrt( mean( cen_std( Y).^2) );
evolve.nLV = NaN;
count = 1;
tr = 1;

if strcmp( opt.Val.met, 'cv')
    cvopt = cv;
    if opt.nLV == -1
        cvopt.nLV = 30;
    else
        cvopt.nLV = opt.nLV;
    end
    cvopt.xpp.met = opt.PP.met;
    cvopt.xpp.set = opt.PP.set;
    cvopt.view = false;
end

count = 2;
evolve.ind( 2, :) = true;

while test
    Xnew = X( :, evolve.ind( count, :));
    evolve.num( count, 1) = sum( evolve.ind( count, :) );
    %Validation method - I should remember pre-processing!
    switch opt.Val.met
        case 'cv'
            for cr = 1:size( opt.Val.ind, 2)
                cvopt.ind = opt.Val.ind( :, cr);
                mod = cv( Xnew, Y, cvopt);
                if opt.nLV == -1
                    rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);                    
                    Btot{ cr} = mod.Val.B;
                    Yptot{ cr} = mod.Val.Yp;
                else
                    rmstemp( cr) = mod.Val.rms( opt.nLV + 1);
                    b( :, cr) = mean( mod.Val.B{end}, 2);
                    Yp( :, cr) = mod.Val.Yp( :, end);
                end
            end
            if opt.nLV == -1
                evolve.nLV( count) = estfac( mod);
                evolve.rms( count) = nanmean( rmstemp( :, evolve.nLV( count) ) );
                for cr = 1:size( opt.Val.ind, 2)
                    b( :, cr) = mean( Btot{ cr}{ evolve.nLV( count)}, 2);
                    Yp( :, cr) = Yptot{ cr}( :, evolve.nLV( count) );
                end
                clear Btot Yptot
            else
                evolve.rms( count) = mean( rmstemp);
                evolve.nLV( count) = opt.nLV;
            end
            b = mean( b, 2);
            Yp = mean( Yp, 2); %Could also just use the calibration Yp, but that would be more or less equal to 'otherwise' below
        case 'boot'
            ind = ceil( rand( r, opt.Val.ind) * r);
            for ci = 1:size( ind, 2)
                Xcal = Xnew( ind( :, ci), :);
                [Xcal,ppset] = prepro( Xcal, opt.PP.met, opt.PP.set);
                Xval = prepro( Xval, opt.PP.met, ppset);
                Ycal = cen_std( Y( ind( :, ci) ) );
                temp = apls( Xcal, Ycal, opt.nLV);
                b( :, ci) = temp( :, end);
                %Two options: 1) Use the predicted Y-values for each
                %                bootstrap sample
                %             2) Predict the unknowns and save these
                %                predictions
                temp = true( r, 1);
                temp( ind( :, ci) ) = false;
                Yp( temp, ci) = plspred( Xval, b( :, ci) );
            end
            Yp( Yp == 0) = NaN;
            Yp = nanmean( Yp, 2)';
            b = mean( b, 2)';
        otherwise
            Xcal = prepr( Xcal, opt.PP.met, opt.PP.set);
            Ycal = cen_std( Y);
            b = apls( Xcal, Ycal, opt.nLV);
            b = b( :, end);
            Yp = Xcal * b + mean( Y);
    end
    
    %Project the samples to the Yp
    tp = Yp/sqrt( b'*b);
    %Project these samples down in the original sample-space
    pp = ( tp' * Xnew/ (tp' * tp))';
    %Calculate the explained and non-explained variance
    res = Xnew - tp * pp';
    ev = var( tp * pp')./ var( Xnew);
    er = var( res)./ var( Xnew);
    clear Yp b
    
    %The selectivity rate (ratio between explained and non-explained)
    selrat = ev./ er;
    ind = find( evolve.ind( count, :));
    %Check the opt.Set(2) value for the first iteration
    if count == 1
        %Adjust it if all or no variables have been selected
            n = 3;
        while (sum( selrat < opt.Set(2) ) == 0 || sum( selrat < opt.Set(2) ) == c) && n >= 0
            opt.Set(2) = mean( selrat) + n * std( selrat);
            n = n - 1;
        end
    end
    count = count + 1;
    evolve.ind( count, :) = evolve.ind( count - 1, :);
%     evolve.ind( count, evolve.ind( count - 1, :)) = selrat < opt.Set(2);
%     
    evolve.ind( count, ind( selrat < opt.Set(2) ) ) = false;
    if sum( evolve.ind( count, :) ) == 0
        test = false;
    end
    if evolve.num( count - 1) == evolve.num( count-2)
        test = false;
    end
    if count > opt.Set(1)
        test = false;
    end
    if isinf( opt.Set(1) )
        if evolve.rms( count - 1) > evolve.rms( count - 2)
            tr = tr + 1;
            if tr > 2
                test = false;
            end
        else
            tr = 1;
        end
    end
%     disp( num( count, :) )
end

if size( evolve.ind, 1) > length( evolve.num)
    evolve.ind( end, :) = [];
end

if isinf( opt.Set(1) )
    [temp, evolve.optimal] = min( evolve.rms );
else
    evolve.optimal = length( evolve.rms);
end
Xnew = X( :, evolve.ind( evolve.optimal, :) );
for cr = 1:size( opt.Val.ind, 2)
    model( cr) = cv( Xnew, Y, cvopt);
    model( cr).ind = evolve.ind( evolve.optimal, :);    
end