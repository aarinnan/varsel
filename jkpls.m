function [ model, evolve] = jkpls( X, Y, opt)
% [ model, evolve] = jkpls( X, Y, opt)
%
%INPUT:
% X      X-Matrix, with samples in rows
% Y      Y-matrix, only good for one Y at the moment
% opt    Type opt = jkpls to get default settings
%
%OUTPUT:
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

% 261111 AAR Added sample numbers as to take these properly into account
% 050310 AAR

%Based on the article: H. Martens, M. Martens (2000): Modified Jack-knife
% estimation of parameter uncertainty in bilinear modeling by partial least
% squares regression (PLSR), Food Quality and Preference, 11, 5-16

if nargin == 0
    model = bcosel;
    model.Method = 'jkpls';
    model.Set = [Inf .01 10];
    model.Info{6} = 'Set - Three numbers: Maximum number of iterations (Inf - optimal)';
    model.Info{7} = '      AND Limit for significance test, AND max percent';
    model.Info{8} = '      of variables removed in one shot';
    return
end

%This should be done using cross-validation. If this has not been set, it
%is changed here with a message to the user
opt.Val.met = lower( opt.Val.met);
if ~strcmp( opt.Val.met, 'cv')
    disp( 'This function only works with cross-validation')
    opt.Val.met = 'cv';
end

if isnan( opt.Val.rep)
    opt.Val.rep = (1:size( X, 1) )';
end

if isnan( opt.Val.ind)
    opt.Val.ind = 1:size( X, 1);
end

%Setting the Set parameter
if length( opt.Set) ~= 3
    opt.Set = [100 .01 10];
end
if opt.Set(1) == -1
    opt.Set(1) = 100;
end
if opt.Set(2) == -1
    opt.Set(2) = .01;
end

%Convert from percent to fraction
opt.Set(3) = opt.Set(3)/ 100;

cvopt = cv;
if opt.nLV == -1
    cvopt.nLV = min( size( X)) - 1;
    if any( strcmp( opt.PP.met, 'as'))
        cvopt.nLV = cvopt.nLV - 1;
    end
else
    cvopt.nLV = opt.nLV;
end
cvopt.xpp.met = opt.PP.met;
cvopt.xpp.set = opt.PP.set;
cvopt.view = false;
cvopt.Val.rep = opt.Val.rep;

[r, c] = size( X);
%Make this an iterative procedure

test = true;
evolve.ind = true( 1, c);
evolve.p = zeros( 1, c);
count = 1;
tr = 0;
id = zeros( 1, c);

evolve.rms = sqrt( mean( cen_std( Y).^2) );
evolve.nLV = NaN;

while test && count < opt.Set(1)
    extest = true;
    Xnew = X( :, evolve.ind( count, :));    
    switch opt.Val.met
        case 'cv'
            b = zeros( sum( evolve.ind( count, :)), length( fjernlike( opt.Val.ind( :, 1) ) ) * size( opt.Val.ind, 2) );
            ind = vec( ones( length( fjernlike( opt.Val.ind( :, 1) ) ), 1) * (1:size( opt.Val.ind, 2) ) );
            for cr = 1:size( opt.Val.ind, 2)
                cvopt.Val.ind = opt.Val.ind( :, cr);
                mod = cv( Xnew, Y, cvopt);
                %Search for the optimal number of LVs
                if opt.nLV == -1
                    rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);
                    Ball{cr} = mod.Val.B;
                else
                    b( :, ind == cr) = mod.Val.B{end};
                    rmstemp( cr) = mod.Val.rms( end);
                end
            end
            %Search for the optimal number of LVs
            if opt.nLV == -1
                rmstemp( rmstemp == 0) = NaN;
                evolve.nLV( count + 1) = estfac( mod); %Should be expanded to allow for repeated cv
                evolve.rms( count + 1) = nanmean( rmstemp( :, evolve.nLV( count + 1) ) );
                for cr = 1:size( opt.Val.ind, 2)
                    b( :, ind == cr) = Ball{cr}{evolve.nLV( count + 1)};
                end
                clear rmstemp Ball
            else
                evolve.rms( count + 1) = mean( rmstemp);
                evolve.nLV( count + 1) = opt.nLV;
                clear rmstemp
            end            
            bcal = mod.Cal.B( :, end);
        case 'boot' %Not optimized
            ind = ceil( rand( r, opt.Val.ind) * r);
            for ci = 1:size( ind, 2)
                Xcal = Xnew( ind( :, ci), :);
                Xcal = prepro( Xcal, opt.PP.met, opt.PP.set);
                Ycal = cen_std( Y( ind( :, ci), : ) );
                
                temp = apls( Xcal, Ycal, opt.nLV);
                b( :, ci) = temp( :, end);
            end
            Xcal = prepro( Xnew, opt.PP.met, opt.PP.set);
            Ycal = cen_std( Y);
            
            bcal = apls( Xcal, Ycal, opt.nLV);
            bcal = bcal( :, end);
        otherwise
            error( '''jkpls'' only works with either ''cv'' or ''boot'' validation')
    end
    
    if evolve.rms( count + 1) > evolve.rms( count) && isinf( opt.Set(1) )
        %Testing if the maximum number of removed variables in each
        %iteration is 100% or (I don't understand the first statement)
        if ceil( c * opt.Set(3) ) == 1 || opt.Set(3) == 1 
            tr = tr + 1;
            if tr > 1
                test = false;
                count = count - 1;
            end
        else
            %Reduce the number of variables removed in each iteration.
            %Include some of the variables that were removed in the
            %previous iteration
            opt.Set(3) = opt.Set(3) / 2;            
            count = count + 1;
            %Go two steps back (as the last iteration did not improve
            %things)
            if count == 2
                test = false;
                count = count - 1;
                evolve.per = opt.Set(3);
            else
                evolve.p( count, :) = evolve.p( count - 2, :);
                %Find the variables that were in the second last model
                fw = find( evolve.ind( count - 2, :));
                %Sort the t-test values
                [ temp, id] = sort( s, 1, 'descend');
                if length( fw) ~= length( s)
                    %I need to go back even one further
                    evolve.p( count, :) = evolve.p( count - 3, :);
                    fw = find( evolve.ind( count - 3, :));
                end
                %Remove only the worst of those variables
                evolve.p( count, fw( id( 1:ceil( opt.Set(3) * c)))) = temp( 1:ceil( opt.Set(3) * c));
                %             if sum( id( ceil( opt.Set(3) * c + 1):end) ) > 0
                %                 evolve.p( count, fw( id( ceil( opt.Set(3) * c + 1):end) ) ) = 0;
                %             end
                evolve.ind( count, :) = evolve.ind( count - 1, :);
                evolve.ind( count, evolve.p( count, :) == 0) = true;
                evolve.per( count) = opt.Set(3);
                extest = false;
            end
        end
    else
        tr = 0;
    end
    
    if sum( evolve.ind( count, :) == 1) == 0
        test = false;
    end
    
    if test && extest
        count = count + 1;
        %According to Martens and Martens
        %Equation #6, estimating the uncertainty variance:
        st = ones( size( b, 2), 1 ) * bcal' - b';
        st = sqrt( sum( st.^2) * (r-1)/r^2);

        %AAR 240418: Update according to new version of ttest
        [h, s] = ttest( [mean( b, 2) st' ones( size( b, 1), 1) * size( b, 2)]);
        S{count} = s;
        [temp, id] = sort( s, 1, 'descend');
        %Testing if any variables are above the significance level
        if any( temp > opt.Set(2))
            fw = find( evolve.ind( count - 1, :));
            id = id( temp > opt.Set(2) );
            id = id( 1:min( length( id), ceil( c * opt.Set(3) ) ) );
            evolve.ind( count, :) = evolve.ind( count - 1, :);
            evolve.ind( count, fw( id) ) = false;
            evolve.p( count, :) = evolve.p( count - 1, :);
            evolve.p( count, fw( id)) = temp( 1:length( id) );
            evolve.per( count) = opt.Set( 3);
            if count > 1
                evolve.p( count, nansum( evolve.p( 1:count-1, :), 1) > 0) = NaN;
            end
        else
            test = false;
        end
        if test
            if sum( evolve.ind( count, :)) == 1 %Stops if there is only one variable left
                test = false;
            end
        end
    end
end
evolve.num = sum( evolve.ind, 2);
if isinf( opt.Set(1) )
    [temp, evolve.optimal] = min( evolve.rms( 2:end) );
else
    evolve.optimal = length( evolve.num);
end
% evolve.p( count, fw) = s;
% evolve.p( count, nansum( evolve.p( 1:count-1, :) ) > 0) = NaN;
Xnew = X( :, evolve.ind( evolve.optimal, :));
for cr = 1:size( opt.Val.ind, 2)
    cvopt.ind = opt.Val.ind( :, cr);
    model(cr) = cv( Xnew, Y, cvopt);
    model(cr).ind = evolve.ind( evolve.optimal, :);
end