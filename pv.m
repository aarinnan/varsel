function [ model, evolve] = pv( X, Y, opt)
% [ model, evolve] = pv( X, Y, opt)
%
%INPUT:
% X      X-Matrix, with samples in rows
% Y      Y-matrix, only good for one Y at the moment
% opt    Type opt = pv to get default settings
%
%OUTPUT:
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

% AAR 280524 Some updates. In order to find the best next variable, the
%             data is orthogonalized with what has already been found
%             (which is how it also should have been coded)
% AAR 100211

if nargin == 0
    model = bcosel;
    model.Method = 'pv';
    model.Set = [ Inf 1];
    model.Info{6} = 'Set - Two numbers: ';
    model.Info{7} = '       1 - Number of iterations. Inf (default) = best';
    model.Info{8} = '       2 - Based on correlation (1) or covariance (2). Default = 1';
    return
end

if opt.Set(2) < 1 && opt.Set(2) > 2
    error( '''opt.Set(2)'' has to be either a 1 (corr) or a 2 (cov)')
end

opt.Val.met = lower( opt.Val.met);
if strcmp( opt.Val.met, 'cv')
    if isnan( opt.Val.ind)
        opt.Val.ind = 1:size( X, 1); %Make LOO-CV
    end
    cvopt = cv;
    if opt.nLV == -1
        cvopt.nLV = 30;
    else
        cvopt.nLV = opt.nLV;
    end
    cvopt.xpp.met = opt.PP.met;
    cvopt.xpp.set = opt.PP.set;
    cvopt.view = false;    
end

[r, c] = size( X);
test = true;
evolve.w = ones( 1, c);
evolve.ind = false( 1, c);
evolve.num = 0;
evolve.rms = sqrt( mean( cen_std( Y).^2) );
evolve.nLV = NaN;

% ex = w;
count = 1;
tf = 0;
tr = 1;

Xnew = X;
Ynew = Y;
while test && count < c
    if opt.Set(2) == 1
        cc = corrcoef( [Xnew Ynew]);
    else
        cc = cov( [ Xnew Ynew]);
    end
    
    %I would like to select a new variable
    cctemp = abs( cc( 1:end-1, end));    
    cctemp( evolve.ind( count, :)) = 0;
    
    [temp, id] = max( cctemp); %Find the next PV
    count = count + 1;
    evolve.ind( count, :) = evolve.ind( count - 1, :);
    evolve.ind( count, id) = true;
    
    Xcal = X( :, evolve.ind( count, :) );
    %Validation method
    switch opt.Val.met
        case 'cv'
            %Make it work with repeated CV
            for cr = 1:size( opt.Val.ind, 2)
                cvopt.ind = opt.Val.ind( :, cr);
                %X is weighted according to B
                mod = cv( Xcal, Y, cvopt);
                %Search for the optimal number of LVs
                if opt.nLV == -1
                    rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);
                else
                    rmstemp( cr) = mod.Val.rms( end);
                end
            end
            %Search for the optimal number of LVs
            if opt.nLV == -1
                rmstemp( rmstemp == 0) = NaN;
                [ evolve.rms( count), evolve.nLV( count)] = min( mod.Val.rms( 2:end) );
%                 evolve.nLV( count) = estfac( mod);
%                 evolve.rms( count) = nanmean( rmstemp( :, evolve.nLV( count) ) );
                clear rmstemp
            else
                evolve.rms( count) = rmstemp( min( opt.nLV, length( rmstemp)) );
                evolve.nLV( count) = min( opt.nLV, length( rmstemp) );
                clear rmstemp
            end
        case 'boot' %Not optimized
            ind = ceil( rand( r, opt.Val.ind) * r);
            for cr = 1:size( ind, 2)
                Xcal = Xnew( ind( :, ci), :);
                Xcal = prepro( Xcal, opt.PP.met, opt.PP.set);
                Ycal = cen_std( Y( ind( :, cr) ) );
                
                temp = apls( Xcal, Ycal, opt.nLV);
                b( :, cr) = temp( :, end);
            end
            b = mean( b, 2); %Only extract the average b-values
        case 'test'
        otherwise   
            Xcal = prepr( Xnew, opt.PP.met, opt.PP.set);
            Ycal = cen_std( Y);
            
            b = apls( Xcal, Ycal, opt.nLV);
            b = b( :, end);
    end
    
    evolve.num( count) = sum( evolve.ind( count, :) );
    %Check if there is no improved model performance
    if isinf( opt.Set(1) )
        if evolve.rms( count) > evolve.rms( count - 1)
            tr = tr + 1;
            if tr > 2
                test = false;
            end
        else
            tr = 1;
        end
    end
    
    if opt.Set(1) < count
        test = false;
    end
    
    if test
        Xnew = X;
        %The selected columns so far
        Xpv = Xnew( :, evolve.ind( count, :));
        Ppv = Xnew' * pinv( Xpv');
        Xnew = Xnew - Xpv * Ppv';
%         Xnew( :, evolve.ind( count, :) ) = NaN;
        Ynew = Y - mod.Cal.Yp( :, evolve.nLV( count) );
    end
end
if isinf( opt.Set(1))
    [temp, evolve.optimal] = min( evolve.rms( 2:end), [], 2);
else
    evolve.optimal = size( evolve.ind, 1) - 1;
end
Xnew = X( :, evolve.ind( evolve.optimal + 1, :));
for cr = 1:size( opt.Val.ind, 2)
    cvopt.ind = opt.Val.ind( :, cr);
    model(cr) = cv( Xnew, Y, cvopt);
    model(cr).ind = evolve.ind( evolve.optimal + 1, :);
end