function [ model, evolve] = rpls( X, Y, opt)
% [ model, evolve] = rpls( X, Y, opt)
%
%INPUT:
% X      X-Matrix, with samples in rows
% Y      Y-matrix, only good for one Y at the moment
% opt    Type opt = rpls to get default settings
%
%OUTPUT:
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

% AAR 080413 Stop if there is no difference in the weights 
%             sum( abs( diff( w) ) ) < 1e-8
% AAR 090712 Store all RMSECV values during each iteration
% AAR 250111 Made it search for the optimal number of nLVs in each
%             iteration, and stop when no improvement of model achieved
% AAR 100111 Adding bootstrapping, test-set and repeated CV
% AAR 021009

if nargin == 0
    model = bcosel;
    model.View = false;
    model.class = false;
    model.Info{6} = 'Set - Three numbers. [Number of iterations, Limit of removing a variable, DevOp]';
    model.Info{7} = '      Set(1) = Inf: until no more model improvement, default';
    model.Info{8} = 'class - False if y is continuous (Default)';
    model.Set = [Inf 1e-3 NaN];
    f = fieldnames( model);
    model = orderfields( model, [1:5 8 7 6]);
    return
end

opt.Val.met = lower( opt.Val.met);
if strcmp( opt.Val.met, 'cv')
    if isnan( opt.Val.ind)
        opt.Val.ind = 1:size( X, 1); %Make LOO-CV
    end
    cvopt = cv;
    if opt.nLV == -1
        cvopt.nLV = 30;
    else
        cvopt.nLV = opt.nLV;
    end
    cvopt.xpp.met = opt.PP.met;
    cvopt.xpp.set = opt.PP.set;
    cvopt.view = false;    
    cvopt.class = opt.class;
end

if opt.Set(2) < 0
    opt.Set(2) = 1e-3;
end

[r, c] = size( X);
test = true;
evolve.w = [zeros( 1, c); ones( 1, c)];
evolve.ind = [false( 1, c); true( 1, c)];
evolve.num = [0; c];
if opt.class
    evolve.rms = size( Y, 1)/ size( Y, 2);
else
    evolve.rms = sqrt( mean( cen_std( Y).^2) );
end
evolve.nLV = NaN;
% ex = w;
count = 2;
tf = 0;
tr = 1;

numY = size( Y, 2);
if numY == 1
    oneY = true;
else
    oneY = false;
end

numCount = 0;
while test && count < 100
    Xnew = X.* ( ones( r, 1) * evolve.w( count, :));
    Xcal = X( :, evolve.ind( count, :) );
    Xnew = Xnew( :, evolve.ind( count, :) );
    %Validation method
    switch opt.Val.met
        case 'cv'
            %Make it work with repeated CV
            for cr = 1:size( opt.Val.ind, 2)
                cvopt.Val.ind = opt.Val.ind( :, cr);
                %X is weighted according to B
                mod = cv( Xnew, Y, cvopt);
                %A variable reduced version of X (no weights)
                if size( Xcal, 2) == c
                    modc = mod;
                else
                    modc = cv( Xcal, Y, cvopt);
                end
                %Search for the optimal number of LVs
                if opt.nLV == -1
                    rmscal( cr, 1:length( modc.Val.rms( 2:end) ) ) = modc.Val.rms( 2:end);
                    rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);
                    for cy = 1:numY
                        Ball{ cr, cy} = mod.Val.B;
                    end
                else
                    for cy = 1:numY
                        b{ cr}( :, cy) = nanmean( mod.Val.B{ cy, end}' )'; %Added 091110 AAR
                    end
                    if oneY
                        b = b{1};
                    else
                        error( 'It does not currently run with more than one y')
                    end
                    rmscal( cr) = modc.Val.rms( end);
                    rmstemp( cr) = mod.Val.rms( end);
                end
            end
            %Search for the optimal number of LVs
            if opt.nLV == -1
                rmstemp( rmstemp == 0) = NaN;
                rmscal( rmscal == 0) = NaN;
                [ ~, evolve.nLV( count)] = min( modc.Val.rms( 2:end) );
                cvopt.nLV = evolve.nLV( count);
%                 evolve.nLV( count + 1) = estfac( modc);
                evolve.detail.rmsall( count + 1, 1:length( rmscal) ) = rmscal;
                evolve.rms( count) = nanmean( rmscal( :, evolve.nLV( count + 1) ) );
                fac = estfac( mod);
                for cr = 1:size( opt.Val.ind, 2)
                    for cy = 1:numY
                        b{ cr}( :, cy) = nanmean( Ball{cr}{fac}, 2);
                    end
                end         
                if oneY
                    b{1} = cell2mat( b);
                end
                clear rmstemp Ball
            else
                evolve.rms( count) = mean( rmscal);
                evolve.nLV( count) = opt.nLV;
                clear rmstemp
            end
        case 'boot' %Not optimized
            ind = ceil( rand( r, opt.Val.ind) * r);
            for cr = 1:size( ind, 2)
                Xcal = Xnew( ind( :, ci), :);
                Xcal = prepro( Xcal, opt.PP.met, opt.PP.set);
                Ycal = cen_std( Y( ind( :, cr) ) );
                
                temp = apls( Xcal, Ycal, opt.nLV);
                b( :, cr) = temp( :, end);
            end
            b = mean( b, 2); %Only extract the average b-values
        case 'test'
        otherwise   
            Xcal = prepr( Xnew, opt.PP.met, opt.PP.set);
            Ycal = cen_std( Y);
            
            b = apls( Xcal, Ycal, opt.nLV);
            b = b( :, end);
    end
    
    %Check if there is no improved model performance
    if isinf( opt.Set(1) )
        if evolve.rms( count) > evolve.rms( count - 1)
            tr = tr + 1;
            if tr > 2
                test = false;
            end
        else
            tr = 1;
        end
    end
    
    if opt.View
        fprintf( [num2str( evolve.num( end)) ' ' num2str( evolve.rms( end), '%4.3f') char( 10)])
    end
    
    if test
        count = count + 1;
        b( isnan( b) ) = 0; %Added 091110 AAR
%         for cb = 1:length( b)
%             b{cb}( isnan( b{cb}) ) = 0; 
%         end
        B = zeros( 1, c);
        B( evolve.ind( count - 1, :)) = b;
        evolve.w( count, :) = evolve.w( count - 1, :).* abs( B);
        evolve.w( count, :) = evolve.w( count, :)/ max( evolve.w( count, :) );
        evolve.ind( count, :) = evolve.w( count, :) > opt.Set(2);
        evolve.num( count, 1) = sum( evolve.ind( count, :));

        %See if there is no difference in the weights
        temp = sum( abs( diff( evolve.w( count-1:count, :) ) ) );
        if temp < 1e-8 && evolve.num( count - 1, 1) == evolve.num( count, 1) 
            test = false;
        end
        
        %Check if the number of variables do not decrease further
        if test && ( evolve.num( count) == evolve.num( count-1) && evolve.num( count) > 0)
            if c - evolve.num( count) > cvopt.nLV || opt.nLV == -1 || evolve.num( count) == c
                tf = tf + 1;
                %Try to boost the search a bit
                evolve.w( count, :) = evolve.w( count - 1, :).* abs( B).^(tf); 
                evolve.w( count, :) = evolve.w( count, :)/ max( evolve.w( count, :) );
                evolve.ind( count, :) = evolve.w( count, :) > opt.Set(2);
                evolve.num( count, 1) = sum( evolve.ind( count, :));
%                 if evolve.num( count, 1) < opt.nLV && opt.nLV > 0                    
%                     temp = find( evolve.w( count, :) > 0);
%                     bad = 0;
%                     while length( temp) < opt.nLV
%                         temp = find( evolve.w( count - bad, :) > 0);
%                         bad = bad + 1;
%                     end
%                     [i, j] = sort( evolve.w( count, temp)', 'descend');
%                     evolve.ind( count, :) = 0;
%                     evolve.ind( count, temp( j( 1:opt.nLV) ) ) = 1;
%                     evolve.num( count, 1) = opt.nLV;
% %                     test = false;
%                 end
                if tf > 10
                    evolve.w( end, :) = [];
                    evolve.num( end) = [];
                    evolve.ind( end, :) = [];
                    test = false;
                end
            else
                test = false;
            end
        else
            tf = 0;
        end
        
        if opt.Set(1) > 0 && count > opt.Set(1)
            test = false;
        end
        clear b
    end
    if evolve.num( end) <= ( opt.nLV + 1)
        numCount = numCount + 1;
        if numCount == 2
            evolve.w( end, :) = [];
            evolve.num( end) = [];
            evolve.ind( end, :) = [];
            test = false;
        end
    end
    
end
[evolve.num, id] = fjernlike( evolve.num);
evolve.detail.id = id;
evolve.ind = evolve.ind( id( :, 1), :);
evolve.detail.rms = evolve.rms;
evolve.rms = evolve.rms( id( :, 1));
evolve.nLV = evolve.nLV( id( :, 1));
if isinf( opt.Set(1))
    [temp, evolve.optimal] = min( evolve.rms, [], 2);
else
    evolve.optimal = size( evolve.ind, 1);
end
Xnew = X( :, evolve.ind( evolve.optimal, :));
for cr = 1:size( opt.Val.ind, 2)
    cvopt.ind = opt.Val.ind( :, cr);
    model(cr) = cv( Xnew, Y, cvopt);
    model(cr).ind = evolve.ind( evolve.optimal, :);
end