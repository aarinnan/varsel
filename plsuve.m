function [model, evolve] = plsuve( X, Y, opt)
%[model, evolve] = plsuve( X, Y, opt)
% Uninformative Variable Elimination
%
%INPUT:
% X      X-matrix with samples in rows
% Y      Y-matrix, only for one y
% opt    Type opt = plsuve to get default settings
%
%OUTPUT:
% model  The final PLS-model
% evolve Information on how it has evolved

%060317 AAR As in JK-PLS, I have included that "only" a percentage of
%            variables are removed at any given time
%211009 AAR

%Based on V?tezslav Centner, Desire-Luc Massart, Onno E. de Noord, Sijmen
%          de Jong, Bernard M. Vandeginste and Cecile Sterna (1996): 
%          Elimination of Uninformative Variables for Multivariate 
%          Calibration, Analytical Chemistry, 68, 3851-3858

if nargin == 0
    model = bcosel;
    model.Method = 'uve';
    model.Set = [1e-10 10 Inf];
    model.Ind = NaN;
    f = fieldnames( model);
    model = orderfields( model, f( [1:3 7 4:6]) );
    model.Info{4} = 'Ind - Indicating intervals the data are divided into (should be removed ALL together)';
    model.Info{6} = 'Set - Two numbers';
    model.Info{7} = '       1. Size of random numbers inserted. A larger number removes more variables';
    model.Info{8} = '       2. How many % variables should be removed at a time';
    model.Info{9} = '       3. Should be set to ''Inf'', for now';

%     model.Info = model.Info( [1:3 7 4:6]);
    return
end

opt.Val.met = lower( opt.Val.met);
if strcmp( opt.Val.met, 'cv')
    if isnan( opt.Val.ind)
        opt.Val.ind = 1:size( X, 1); %Make LOO-CV
    end
    cvopt = cv;
    if opt.nLV == -1
        cvopt.nLV = 60;
    else
        cvopt.nLV = opt.nLV;
    end
    cvopt.xpp.met = opt.PP.met;
    cvopt.xpp.set = opt.PP.set;
    cvopt.view = false;   
    cvopt.Val.ind = opt.Val.ind;
    cvvar = cvopt;
end

[r, c] = size( X);
test = true;
count = 0;
num = 0;
evolve.ind = true( 1, c);
evolve.num = 0;
evolve.rms = sqrt( mean( cen_std( Y).^2) );
evolve.nLV = NaN;

%280111 AAR Ok, this requires a bit of coding, but I think it would be
%           "fun" to make this algorithm iterative both with regards to
%           the number of factors in the variable selection, but also in
%           the number of times the variable selection is run -> but not
%           for now

if ~isnan( opt.Ind)
    [ii, ji] = fjernlike( opt.Ind);
    vn = sum( ~isnan( ji), 2);
end

while test && count < opt.Set(3)
    count = count + 1;
    if count > 1
        XR = [X( :, evolve.ind( count - 1, :) ) randn( r, sum( evolve.ind( count - 1, :)) ) * opt.Set( 1)]; %Adding the random variables

        %The original article suggested LOO, but this is overoptimistic and
        %will certainly give some false elimination
        b = zeros( size( XR, 2), length( fjernlike( opt.Val.ind( :, 1) ) ) * size( opt.Val.ind, 2) );
        ind = vec( ones( length( fjernlike( opt.Val.ind( :, 1) ) ), 1) * (1:size( opt.Val.ind, 2) ) );
        
        for cr = 1:size( opt.Val.ind, 2)
            cvvar.ind = opt.Val.ind( :, cr);
            mod(cr) = cv( XR, Y, cvvar);
            if opt.nLV == -1
                nLV( cr) = estfac( mod(cr) );
            else
                nLV( cr) = opt.nLV;
            end
        end
        
        if opt.nLV == -1
            nLV = floor( mean( nLV) );
        else
            nLV = opt.nLV;
        end
        for cr = 1:size( opt.Val.ind, 2)
            b( :, ind == cr) = mod( cr).Val.B{ min( length( mod(cr).Val.B), nLV)};
        end
                    
        b = abs( mean( b, 2))./ std( b, 0, 2);
        lim = max( b( size( XR, 2)/2 + 1:end) );
        evolve.lim( count) = lim;
        
        %Need to check how many in each of the intervals are below the
        %limit. Only those intervals where more than 50% are below are
        %removed
        if ~isnan( opt.Ind)
            %Find the intervals still in the model
            temp = opt.Ind( evolve.ind( count - 1, :) );
            %Remove those variables which are still good
            temp( b( 1:sum( evolve.ind( count - 1, :) ) ) > lim) = [];
            %Estimate how much of each of the remaining intervals which are
            %removed
            evolve.ind( count, :) = evolve.ind( count - 1, :);
            if ~isempty( temp)
                [i, j] = fjernlike( temp);
                i = [i sum( ~isnan( j), 2) vn( i)];
                %Find the intervals which should be removed
                i( i( :, 2)./ i( :, 3) < .5, :) = [];
                temp = ji( i( :, 1), :);
                temp = vec( temp);
                temp( isnan( temp) ) = [];
                evolve.ind( count, temp) = false;
            end
        else
            btemp = b( 1:sum( evolve.ind( count - 1, :) ) );
            if ~isnan( opt.Set(2))
                n = sum( btemp < lim)/ c * 100;
                if n > opt.Set(2)
                    [i, j] = sort( btemp);
                    lim = i( floor( c * opt.Set(2)/ 100) );  
                    evolve.lim( count) = lim;
                end                
            end
            evolve.ind( count, evolve.ind( count - 1, :)) = b( 1:sum( evolve.ind( count - 1, :) )) > lim;
        end
    end
    
    if sum( evolve.ind( count, :) ) == 0
        test = false;
    end
    
    evolve.num( count) = sum( evolve.ind( count, :), 2);
    if test
        for cr = 1:size( opt.Val.ind, 2)
            cvopt.ind = opt.Val.ind( :, cr);
            mod = cv( X( :, evolve.ind( count, :) ), Y, cvopt);
            rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);
        end
        if opt.nLV == -1
            evolve.nLV( count + 1) = estfac( mod(1) );
            evolve.rms( count + 1) = nanmean( rmstemp( :, evolve.nLV( count + 1) ) );
            if count == 1
                cvvar.nLV = evolve.nLV( count + 1);
            end
        else
            evolve.rms( count + 1) = mean( rmstemp( :, min( size( rmstemp, 2), opt.nLV) ) );
            evolve.nLV( count + 1) = min( cvopt.nLV, size( rmstemp, 2) )    ;
        end    
        clear rmstemp
        if count > 1
            if evolve.rms( count + 1) > evolve.rms( count) && opt.Set(3) == Inf
                test = false;
%             else
%                 
%                 num = num + 1;
%                 test = false;
            end
%             if evolve.num( count) == 1
%                 test = false;
%             end
            if evolve.num( count) == evolve.num( count - 1)
                test = false;
                evolve.ind( end, :) = [];
                evolve.num( end) = [];
                evolve.nLV( end) = [];
                evolve.rms( end) = [];
            end
            if cvvar.nLV == 0
                test = false;
            end
        end
    end
end
%The size of 'evolve' is slightly wrong
evolve.ind = [false( 1, c); evolve.ind];
evolve.num = [0 evolve.num];
% evolve.num = sum( evolve.ind, 2);
if isinf( opt.Set(3) )
    [temp, evolve.optimal] = min( evolve.rms);
else
    evolve.optimal = length( evolve.num);
end
Xnew = X( :, evolve.ind( evolve.optimal, :) );
for cr = 1:size( opt.Val.ind, 2)
    cvopt.ind = opt.Val.ind( :, cr);
    model( cr) = cv( Xnew, Y, cvopt);
    model( cr).ind = evolve.ind( evolve.optimal, :);
end