function [model, evolve] = ipls( X, Y, opt)
%[model, evolve] = ipls( X, Y, opt);
%
% INPUT:
%  X        X-matrix
%  Y        Y-variable to be investigated
%  opt      Type opt = ipls to get default settings
%
% OUTPUT
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

%Based on the article: L. N�rgaard, A. Saudland, J. Wagner, J. P. Nielsen,
%  L. Munck, S. B. Engelsen (2000): Interval Partial Least-Squares Regression
%  (iPLS): A Comparative Chemometric Study with an Example from Near-Infrared
%  Spectroscopy, Applied Spectroscopy, 54, 413-419

% 100214 AAR It didn't really work when the number of intervals were set to
%             Inf. This should now work.
% 240713 AAR Made a lot of changes -> bipls & made it work better with
%             missing values, to mention a few
% 120111 AAR Made it fit to bcosel. However, only works with CV at the
%             moment
% 010210 AAR Updated the input to 'cv', nut need to also update the
%             preprocessing part
% 171108 AAR Made a shortcut with regards to missing values. It simply
%             takes too long if I don't add this feature
% 141108 AAR


if nargin == 0
    model = bcosel;
    model.Ind = NaN;
    model.Method = 'ipls';
    model.Set = [1 0 1];
    model.View = true;
    model.Info{4} = 'Ind - # of intervals to be made, or index with equal number for each interval';
    model.Info{6} = 'Set - Three numbers:';
    model.Info{7} = '       Backward(0) or Forward(1) selection';
    model.Info{8} = '       # of intervals: 0 = all 1 interval models & Inf = optimal # of intervals';
    model.Info{9} = '       Continous Y (1) or classification (0)';
    model.Info{10} = 'View - True if you want to see the progress, false if not';
    model = orderfields( model, {'PP', 'nLV', 'Method', 'Ind', 'Val', 'Set', 'View', 'Info'} );
    return
end

[r, c] = size(X);

if isnan( opt.Val.rep)
    opt.Val.rep = (1:size( X, 1) )';
end

opt.Val.met = lower( opt.Val.met);
if strcmp( opt.Val.met, 'cv')
    if isnan( opt.Val.ind)
        opt.Val.ind = opt.Val.rep; %Make LOO-CV
    end
    cvopt = cv;
    cvopt.nLV = opt.nLV;
    cvopt.xpp.met = opt.PP.met;
    cvopt.xpp.set = opt.PP.set;
    cvopt.view = false;  
    cvopt.Val.rep = opt.Val.rep;
    cvopt.Val.ind = opt.Val.ind;
end

if isnan( opt.Ind) % Make an automatic interval assignment if no input    
    opt.Ind = makeint( 20, c);
elseif length( opt.Ind) == 1
    opt.Ind = makeint( opt.Ind, c);
else
    if length( opt.Ind) ~= c
        error('''opt.Ind'' should have the same length as columns in ''x''')
    end    
end

if opt.Set(1) < 0 || opt.Set(1) > 1
    opt.Set(1) = 1; %Default value is 'forward selection'
end

if opt.Set(2) < 0
    opt.Set(2) = 0; %Default is only using one interval
end

if opt.Set(3) < 0 || opt.Set(3) > 1
    opt.Set(3) = 1;
end

if strcmp( opt.Val.met, 'cv')
    cvopt.class = opt.Set(3) == 0;
end

if sum( isnan( vec( X) ) ) > 0
    disp('  There are missing data in the matrix. These data will be estimated by a')
    disp('  global model, in order to speed up the later calculations')
    Xcal = prepro( X, opt.PP.met, opt.PP.set);
    Ycal = cen_std( Y);
    [~, xnew] = plsmiss( Xcal, Ycal, opt.nLV);
%     xnew = t * p';
    mis = true;
else
    mis = false;
end

% Model.int = int;

if opt.Set(3) == 0
    rmgoal = size( Y, 1) * .5;
else
    rmgoal = sqrt( mean( cen_std( Y).^2 ) );
end
cont = true;

num = 0;
sin = [];

si = false( c, 1); %Logical variable axis
ni = 1; %Number of intervals selected

while cont
    if opt.View
        h = waitbar( 0, 'iPLS is running');
    end
    %Don't need to test those intervals already selected!    
    rms = ones( 1, length( fjernlike( opt.Ind) )) * NaN;
    no = ones( 1, length( fjernlike( opt.Ind) )) * NaN;
    inter = fjernlike( opt.Ind( si == 0) );
    inter( inter == 0) = [];
    
    x = X;
    if mis %Speed up the calculations with missing data
        x( isnan( X) ) = xnew( isnan( X) );
    end
       
    for ci = vec(inter)'
        if opt.View
            waitbar( ci/ max( inter), h)
        end
        tsi = si;
        tsi( opt.Ind == ci) = true;
        xcal = x( :, tsi == opt.Set(1) );
%         fac = min( [nLV 20 size( xcal) ] );
        mod = cv( xcal, Y, cvopt);
        %Should do a different minimization if this is a classification
        %problem
        mod.nLV = estfac( mod);
%         [temp, mod.nLV] = min( mod.Val.rms( 2:end) );
%         mod.nLV = numfac( mod );
        if opt.Set(2) == 0
            model(ci) = mod;
        end
        rms(ci) = mod.Val.rms( mod.nLV + 1);
        no(ci) = mod.nLV;
    end
    if opt.View
        close( h)
    end
    [temp, best] = nanmin( rms, [], 2);
    no = no(best);      
    
    if mis
        tsi = si;
        tsi( opt.Ind == best) = true;
        xcal = X( :, tsi);
        tempid = sum( isnan( xcal), 2) < size( xcal, 2);
        if any( ~tempid)            
            newopt = cvopt;
            newopt.Val.ind = cvopt.Val.ind( tempid);
            if length( cvopt.Val.rep) == length( cvopt.Val.ind)
                newopt.Val.rep = cvopt.Val.ind( tempid);
            end
            newopt.Val.rep
            xred = xcal( tempid, :);
            yred = Y( tempid);
            mod = cv( xred, yred, newopt);
        else
            mod = cv( xcal, Y, cvopt);
        end
        [temp, no] = min( mod.Val.rms( 2:end) );
        no = min( size( mod.Cal.Yp, 2), no);
        if iscell( mod.Cal.T)
            xnew( tempid, tsi) = mod.Cal.T{no} * mod.Cal.P{no}';
        else
            xnew( tempid, tsi) = mod.Cal.T( :, 1:no) * mod.Cal.P( :, 1:no)';
        end
    end

    if opt.View
        fprintf( [num2str( ni) ': ' num2str( no) ' - ' num2str( temp, '%3.5f') '\n'] )
    end
    
    if opt.Set(2) < Inf
        if opt.Set(2) == ni || opt.Set(2) == 0
            cont = false;
        else
            si( opt.Ind == best) = true;
        end
    else
        si( opt.Ind == best) = true;
        evo.ind( ni, :) = si;
        evo.nLV( ni) = no;
        evo.rms( ni) = temp;
        try
            evo.int( ni, :) = rms;
        catch
            evo.int( ni, :) = [ rms ones( 1, size( evo.int, 2) - length( rms)) * NaN];
        end
        if temp >= rmgoal %Should code something here, so the best selection comes out as the final model
            num = num + 1;
            if num == 2
                %The best intervals
                fprintf( [ 'Taking two steps back, and returning the optimal model' char( 10)])
                [~, numin] = min( evo.rms);
                evo.Optimal = numin;
                si = evo.ind( numin, :);
%                 si = false( length( si), 1);
%                 for ci = selin
%                     si( opt.Ind == ci) = true;
%                 end
                cont = false;
%             else
%                 si( opt.Ind == best) = true;
%                 sin = [sin; best no temp];
            end
        else
            num = 0;
            rmgoal = temp;
%             si( opt.Ind == best) = true;
%             sin = [sin; best no temp];
        end
    end
    ni = ni + 1;
    
end

if opt.Set(2) > 0
       %I would like to see how it looks like if we go too far as well
%     if opt.Set(2) == Inf
%         si( opt.Ind == sin(end, 1) ) = false;
%         sin(end, :) = [];
%     end
    xcal = X( :, si == opt.Set(1));
    if ~isempty( xcal)
        id = sum( isnan( xcal), 2) < size( xcal, 2);
        cvopt.Val.ind = cvopt.Val.ind( id);
        if length( cvopt.Val.rep) == length( id)
            cvopt.Val.rep = cvopt.Val.rep( id);
        end
%         fac = min( [nLV 20 size( xcal) ] );
        model = cv( xcal( id, :), Y( id), cvopt);
        model.ind = si == opt.Set(1);
        evolve = evo; 
    else
        disp('iPLS with the current settings could not improve the model')
    end
else
    if ~exist( 'evolve')
        evolve = NaN;
    end
end
