function [ model, evolve] = vip( X, Y, opt)
% [ model, evolve] = vip( X, Y, opt)
%
%INPUT:
% X      X-Matrix, with samples in rows
% Y      Y-matrix, only good for one Y at the moment
% opt    Type opt = vip to get default settings
%
%OUTPUT:
% model  Includes the model parameters for the model
% evolve Information on how the model has evolved

% AAR 061010

%Based on the articles: 
% - Svante Wold, Michael Sj�str�m, Lennart Eriksson (2001): PLS-regression:
%   a basic tool of chemometrics, Chemometrics and Intelligent Laboratory
%   Systems, 58, 109-130
% - Il-Gyo Chong, Chi-Hyuck Jun (2005): Performance of some variable 
%   selection methods when multicollinearity is present, Chemometrics 
%   and Intelligen Laboratory Systems, 78, 103-112


if nargin == 0
    model = bcosel;
    model.Method = 'vip';
    model.Set = [Inf 1];
    model.Info{6} = 'Set - Number of iterations (Inf = optimal)';
    model.Info{7} = '      Limit for VIP-variables (1 = default)';
    return
end

%This should be done using cross-validation. If this has not been set, it
%is changed here with a message to the user
opt.Val.met = lower( opt.Val.met);
if ~strcmp( opt.Val.met, 'cv')
    disp( 'This function only works with cross-validation')
    opt.Val.met = 'cv';
end
if isnan( opt.Val.ind)
    opt.Val.Setting = 1:size( X, 1);
end
cvopt = cv;
if opt.nLV == -1
    cvopt.nLV = 30;
else
    cvopt.nLV = opt.nLV;
end
cvopt.xpp.met = opt.PP.met;
cvopt.xpp.set = opt.PP.set;
cvopt.view = false;

[r, c] = size( X);
%Make this an iterative procedure

test = true;
evolve.ind = true( 1, c);
evolve.vip = ones( 1, c);
count = 1;
tr = 1;

evolve.rms = sqrt( mean( cen_std( Y).^2) );
evolve.nLV = NaN;

while test && count < opt.Set(1)
    Xnew = X( :, evolve.ind( count, :));
    switch opt.Val.met
        case 'cv'
            for cr = 1:size( opt.Val.ind, 2)
                cvopt.ind = opt.Val.ind( :, cr);
                mod = cv( Xnew, Y, cvopt);
                rmstemp( cr, 1:length( mod.Val.rms( 2:end) ) ) = mod.Val.rms( 2:end);
            end
            if opt.nLV == -1
                evolve.nLV( count + 1) = estfac( mod( 1) );
                evolve.rms( count + 1) = nanmean( rmstemp( :, evolve.nLV( count + 1) ) );
            else
                evolve.nLV( count + 1) = min( opt.nLV, size( Xnew, 2) );
                evolve.rms( count + 1) = nanmean( rmstemp( :, evolve.nLV( count + 1)), 1);
            end
            clear rmstemp
            Yp = mod.Cal.Yp( :, evolve.nLV( count + 1) );
            T = mod.Cal.T( :, 1:evolve.nLV( count + 1) );
            W = mod.Cal.W( :, 1:evolve.nLV( count + 1) );
        case 'none'
                Xcal = prepro( Xnew, opt.PP.met, opt.PP.set);
                [B, W, T] = apls( xcal, cen_std( Y), opt.nLV);
                Yp = plspred( Xcal, B, [], mean( Y));
    end
    
    if isinf( opt.Set(1) )
        if evolve.rms( count + 1) > evolve.rms( count)
            tr = tr + 1;
            if tr > 2
                test = false;
            end
        else
            tr = 1;
        end
    end
    
    if test
        %According Chong & Jun
        %Would be fun to try the validated scores instead. However, that
        %would mean that they should be rotated ... That's for later
        tnorm = diag( T' * T);
        b = pinv( T) * Yp;
        b = b( :, end);
        ev = b.^2 .* tnorm;
        vips = sum( evolve.ind( count, :) ) * W.^2 * ev./sum(ev);
        %Chong & Jun stated (I think, should verify) that 1 should be the
        %limit. In the case of iteration, a limit of 0.85 seems optimal (for
        %beer at least)
        id = vips < opt.Set(2); %It seems that this limit is not optimal!!
        count = count + 1;
        if any( id)
            fw = find( evolve.ind( count - 1, :) );
            evolve.ind( count, :) = evolve.ind( count - 1, :);
            evolve.ind( count, fw( id) ) = false;
            evolve.vip( count, :) = evolve.vip( count - 1, :);
            evolve.vip( count, fw) = vips;
            if sum( evolve.ind( count, :)) == 0
                test = false;
                evolve.ind( count, :) = [];
            end
        else
            test = false;
        end
    end
end
evolve.num = sum( evolve.ind, 2);
if isinf( opt.Set(1))
    [temp, evolve.optimal] = min( evolve.rms( 2:end) );
    Xnew = X( :, evolve.ind( evolve.optimal, :) );
else
    evolve.optimal = length( evolve.num);
    Xnew = X( :, evolve.ind( end, :) );
end
for cr = 1:size( opt.Val.ind, 2)
    cvopt.ind = opt.Val.ind( :, cr);
    model(cr) = cv( Xnew, Y, cvopt);
    model(cr).ind = evolve.ind( evolve.optimal, :);
end